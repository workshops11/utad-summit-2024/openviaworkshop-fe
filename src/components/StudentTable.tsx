"use client";
import {
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableHeader,
    TableRow,
} from "@/components/ui/table"
import createApolloClient from '@/services/graphl';
import { gql } from '@apollo/client';
import { useEffect, useState } from 'react';
interface Student {
    id:  string | number;
    name: string;
    number: number;
}
export function StudentTable() {
    const client = createApolloClient();
    const [students, setStudents] = useState<Student[]>([]);
    useEffect(() => {
        async function fetchData() {
            const { data } = await client.query({
                query: gql`
                    query students {
                        students {
                            id
                            name
                            number
                        }
                    }
                `,
            });
            setStudents(data.students);
        }
        fetchData();
    }, []);

    return (
        <Table>
            <TableHeader>
                <TableRow>
                    <TableHead>Al</TableHead>
                    <TableHead>Name</TableHead>
                </TableRow>
            </TableHeader>
            <TableBody>
                {students.map((student:Student) => (
                    <TableRow key={student.id}>
                        <TableCell>{student.number}</TableCell>
                        <TableCell>{student.name}</TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    )
}
