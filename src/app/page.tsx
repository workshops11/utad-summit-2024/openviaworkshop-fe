import { StudentDialog } from '@/components/StudentDialog';
import { StudentTable } from '@/components/StudentTable';

export default function Home() {  

  return (
    <main className="flex gap-2 flex-col items-center justify-between p-24">
      <header className="flex justify-between w-full">
        <h1 className="text-3xl font-bold">Students</h1>
        <StudentDialog />
      </header>
      <StudentTable />
    </main>
  );
}
